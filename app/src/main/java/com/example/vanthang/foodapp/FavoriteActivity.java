package com.example.vanthang.foodapp;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.vanthang.foodapp.Adapter.CartAdapter;
import com.example.vanthang.foodapp.Adapter.FavoriteAdapter;
import com.example.vanthang.foodapp.Database.Database;
import com.example.vanthang.foodapp.Helper.RecyclerItemTouchHelper;
import com.example.vanthang.foodapp.Interface.ItemOnClickListener;
import com.example.vanthang.foodapp.Interface.RecyclerItemTouchHelperListener;
import com.example.vanthang.foodapp.Model.Favorite;
import com.example.vanthang.foodapp.Model.Food;
import com.example.vanthang.foodapp.Model.Order;
import com.example.vanthang.foodapp.Utils.Common;
import com.example.vanthang.foodapp.ViewHolder.FoodViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FavoriteActivity extends AppCompatActivity implements RecyclerItemTouchHelperListener {
    RecyclerView rw_food_list;
    RelativeLayout rootLayout;
    FavoriteAdapter adapter;
    //favorites
    List<Favorite> favoriteList=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        //view
        rootLayout=findViewById(R.id.rootlayout);
        rw_food_list=findViewById(R.id.rw_food_list);
        rw_food_list.hasFixedSize();
        rw_food_list.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        LayoutAnimationController controller= AnimationUtils.loadLayoutAnimation(this,R.anim.layout_fall_down);
        rw_food_list.setLayoutAnimation(controller);
        //swipe delete
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback=new RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT,this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rw_food_list);

        loadAllFavorites();

    }

    private void loadAllFavorites() {
        favoriteList=new Database(this).getAllFavorites(Common.current_User.getPhone());
        adapter=new FavoriteAdapter(FavoriteActivity.this,favoriteList);
        rw_food_list.setAdapter(adapter);
    }

    @Override
    public void onSwipe(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof FavoriteAdapter.ViewHolder){
            String name=adapter.getItem(viewHolder.getAdapterPosition()).getFoodName();
            final Favorite favorite=adapter.getItem(viewHolder.getAdapterPosition());
            final int itemIndex=viewHolder.getAdapterPosition();

            adapter.removeItem(viewHolder.getAdapterPosition());
            new Database(getApplicationContext()).removeFromFavorites(favorite.getFoodId(),favorite.getUserPhone());
            adapter.notifyDataSetChanged();

            //make snackbar
            Snackbar snackbar=Snackbar.make(rootLayout,name+" removed from cart!",Snackbar.LENGTH_SHORT);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapter.restoreItem(favorite,itemIndex);
                    new Database(getApplicationContext()).addToFavorites(favorite);
                    adapter.notifyDataSetChanged();
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }
}
