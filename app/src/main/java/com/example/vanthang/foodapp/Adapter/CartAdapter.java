package com.example.vanthang.foodapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.vanthang.foodapp.Cart;
import com.example.vanthang.foodapp.Database.Database;
import com.example.vanthang.foodapp.Interface.ItemOnClickListener;
import com.example.vanthang.foodapp.Model.Order;
import com.example.vanthang.foodapp.R;
import com.example.vanthang.foodapp.Utils.Common;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    private Cart cartContext;
    private List<Order> orderList;

    public CartAdapter(Cart context, List<Order> orderList) {
        this.cartContext = context;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public CartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView=LayoutInflater.from(cartContext).inflate(R.layout.cart_item_layout,viewGroup,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CartAdapter.ViewHolder viewHolder, final int i) {
        viewHolder.txt_cart_item_name.setText(orderList.get(i).getProductName());

        Picasso.with(cartContext).load(orderList.get(i).getImage())
                .resize(70,70)
                .into(viewHolder.img_cart);

        Locale locale=new Locale("en","US");
        NumberFormat fmt=NumberFormat.getCurrencyInstance(locale);
        int price=(Integer.parseInt(orderList.get(i).getPrice()))*(Integer.parseInt(orderList.get(i).getQuantity()));
        viewHolder.txt_cart_item_price.setText(fmt.format(price));

//        TextDrawable drawable=TextDrawable.builder()
//                .buildRound(""+orderList.get(i).getQuantity(),Color.RED);
        viewHolder.cart_item_quantity.setOnValueChangeListener(new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                Order order=orderList.get(i);
                order.setQuantity(String.valueOf(newValue));
                new Database(cartContext).updateCart(order);

                //update total price
                int totalPrice = 0;
                List<Order> carts=new Database(cartContext).getCarts(Common.current_User.getPhone());
                for (Order item : carts)
                    totalPrice += (Integer.parseInt(item.getPrice())) * (Integer.parseInt(item.getQuantity()));
                Locale locale = new Locale("en", "US");
                NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
                cartContext.txt_total_price.setText(fmt.format(totalPrice));

            }
        });
        viewHolder.cart_item_quantity.setNumber(orderList.get(i).getQuantity());
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }
    public Order getItem(int position){
        return orderList.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnCreateContextMenuListener{
        public TextView txt_cart_item_name,txt_cart_item_price;
        public ElegantNumberButton cart_item_quantity;
        public ImageView img_cart;
        public RelativeLayout view_background;
        public LinearLayout   view_foreground;

        ItemOnClickListener itemOnClickListener;

        public void setItemOnClickListener(ItemOnClickListener itemOnClickListener) {
            this.itemOnClickListener = itemOnClickListener;
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_cart_item_name=itemView.findViewById(R.id.txt_cart_item_name);
            txt_cart_item_price=itemView.findViewById(R.id.txt_cart_item_price);
            cart_item_quantity=itemView.findViewById(R.id.cart_item_quantity);
            img_cart=itemView.findViewById(R.id.img_cart);
            view_background=(RelativeLayout)itemView.findViewById(R.id.view_background);
            view_foreground=(LinearLayout)itemView.findViewById(R.id.view_foreground);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View view) {
            this.itemOnClickListener.onClick(view,getAdapterPosition(),false);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.setHeaderTitle("Select action");
            contextMenu.add(0,0,getAdapterPosition(),Common.DELETE);
        }
    }
    public void removeItem(int position){
        orderList.remove(position);
        notifyItemRemoved(position);
    }
    public void restoreItem(Order item,int position){
        orderList.add(position,item);
        notifyItemInserted(position);
    }
}
