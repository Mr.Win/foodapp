package com.example.vanthang.foodapp.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.vanthang.foodapp.Model.User;
import com.example.vanthang.foodapp.Retrofit.APIService;
import com.example.vanthang.foodapp.Retrofit.GoogleRetrofitClient;
import com.example.vanthang.foodapp.Retrofit.IGoogleService;
import com.example.vanthang.foodapp.Retrofit.RetrofitClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class Common {
    public static final String topicName="News";
    public static final String PHONE_TEXT ="userPhone";
    public static User current_User;
    public static String DELETE = "Delete";
    public static String USER_KEY = "User";
    public static String PASSWORD_KEY = "Password";
    private static String baseURL="https://fcm.googleapis.com/";
    private static String GOOGLE_API_URL="https://maps.googleapis.com/";
    public static final String FOOD_ID="FoodId";


    public static String convertCodeToStatus(String status) {
        if (status.equals("0"))
            return "Placed";
        else if (status.equals("1"))
            return "On my way";
        else
            return "Shipped";
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo[] infos = connectivityManager.getAllNetworkInfo();
            if (infos != null) {
                for (int i = 0; i < infos.length; i++) {
                    if (infos[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
                }
            }
        }
        return false;
    }

    public static APIService getFCMService(){
        return RetrofitClient.getClient(baseURL).create(APIService.class);
    }
    public static IGoogleService getGoogleMapApi(){
        return GoogleRetrofitClient.getGoogleApi(GOOGLE_API_URL).create(IGoogleService.class);
    }

    //this func convert currency to number base on locale
    public static BigDecimal formatCurrency(String amount, Locale locale) throws java.text.ParseException {
        NumberFormat numberFormat=NumberFormat.getCurrencyInstance(locale);
        if (numberFormat instanceof DecimalFormat){
            ((DecimalFormat)numberFormat).setParseBigDecimal(true);
        }
        return  (BigDecimal)numberFormat.parse(amount.replace("[^\\d.,]",""));
    }

}
