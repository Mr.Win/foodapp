package com.example.vanthang.foodapp.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.vanthang.foodapp.R;

public class ShowCommentViewHolder extends RecyclerView.ViewHolder {
    public TextView txt_userphone;
    public TextView txt_comment;
    public RatingBar ratingBar;
    public ShowCommentViewHolder(@NonNull View itemView) {
        super(itemView);
        txt_userphone=itemView.findViewById(R.id.txt_userphone_showcomment);
        txt_comment=itemView.findViewById(R.id.txt_comment_showcomment);
        ratingBar=itemView.findViewById(R.id.rating_Bar_showcomment);
    }
}
