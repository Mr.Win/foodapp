package com.example.vanthang.foodapp.Interface;

import android.view.View;

public interface ItemOnClickListener {
    public void onClick(View view,int position,boolean isLongClick);
}
