package com.example.vanthang.foodapp.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.vanthang.foodapp.Interface.ItemOnClickListener;
import com.example.vanthang.foodapp.R;

public class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ItemOnClickListener itemOnClickListener;
    public ImageView img_menu_item;
    public TextView txt_menu_item;

    public void setItemOnClickListener(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    public MenuViewHolder(@NonNull View itemView) {
        super(itemView);
        img_menu_item=itemView.findViewById(R.id.img_menu_item);
        txt_menu_item=itemView.findViewById(R.id.txt_menu_item);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        this.itemOnClickListener.onClick(view,getAdapterPosition(),false);
    }
}