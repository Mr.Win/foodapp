package com.example.vanthang.foodapp.Model;

public class User {
    private String Name;
    private String Password;
    private String Phone;
    private Boolean IsStaff;
    private String SecureCode;
    private String HomeAddress;
    private Object balance;

    public User() {
    }

    public User(String name, String password, String phone, Boolean isStaff, String secureCode, String homeAddress) {
        Name = name;
        Password = password;
        Phone = phone;
        IsStaff = isStaff;
        SecureCode = secureCode;
        HomeAddress = homeAddress;

    }

    public Object getBalance() {
        return balance;
    }

    public void setBalance(Object balance) {
        this.balance = balance;
    }

    public String getHomeAddress() {
        return HomeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        HomeAddress = homeAddress;
    }

    public String getSecureCode() {
        return SecureCode;
    }

    public void setSecureCode(String secureCode) {
        SecureCode = secureCode;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public Boolean getStaff() {
        return IsStaff;
    }

    public void setStaff(Boolean staff) {
        IsStaff = staff;
    }
}
