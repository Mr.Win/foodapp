package com.example.vanthang.foodapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.example.vanthang.foodapp.Database.Database;
import com.example.vanthang.foodapp.Interface.ItemOnClickListener;
import com.example.vanthang.foodapp.Model.Banner;
import com.example.vanthang.foodapp.Model.Category;
import com.example.vanthang.foodapp.Model.Favorite;
import com.example.vanthang.foodapp.Model.Token;
import com.example.vanthang.foodapp.Utils.Common;
import com.example.vanthang.foodapp.ViewHolder.MenuViewHolder;
import com.facebook.accountkit.AccountKit;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import io.paperdb.Paper;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FirebaseDatabase db;
    DatabaseReference category,banners;

    TextView txt_NameUser;
    RecyclerView rw_menu;

    FirebaseRecyclerAdapter<Category, MenuViewHolder> adapter;

    SwipeRefreshLayout swipeRefreshLayout;
    CounterFab fab;

    //slider
    HashMap<String,String> listImage;
    SliderLayout sliderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/restaurant_font.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Menu");
        toolbar.setBackgroundColor(Color.BLACK);
        setSupportActionBar(toolbar);

        //init database
        db = FirebaseDatabase.getInstance();
        category = db.getReference().child("Category");

        //dat tai day de load duoc animation
        FirebaseRecyclerOptions<Category> options = new FirebaseRecyclerOptions.Builder<Category>()
                .setQuery(category, Category.class)
                .build();
        adapter = new FirebaseRecyclerAdapter<Category, MenuViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull MenuViewHolder holder, int position, @NonNull Category model) {
                Picasso.with(getApplicationContext())
                        .load(model.getLink())
                        .into(holder.img_menu_item, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
                holder.txt_menu_item.setText(model.getName());
                holder.setItemOnClickListener(new ItemOnClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //get CategoryId and send to FoodList Activity
                        Intent intent = new Intent(Home.this, FoodList.class);
                        intent.putExtra("CategoryId", adapter.getRef(position).getKey());
                        startActivity(intent);

                    }
                });
            }

            @NonNull
            @Override
            public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.menu_item_layout, viewGroup, false);
                return new MenuViewHolder(itemView);
            }
        };

        //paper init
        Paper.init(this);

        fab =  findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Home.this, Cart.class));
            }
        });

        fab.setCount(new Database(getApplicationContext()).getCountCart(Common.current_User.getPhone()));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //view
        swipeRefreshLayout=findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorOrange,
                android.R.color.holo_green_dark,
                android.R.color.holo_purple,
                android.R.color.holo_red_dark,
                R.color.colorPrimary,
                android.R.color.holo_blue_dark);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Common.isConnectedToInternet(getApplicationContext()))
                    loadMenu();
                else {
                    Toast.makeText(getApplicationContext(), "Please check your connection !!", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                    return;
                }
            }
        });
        //default ,load for first time
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (Common.isConnectedToInternet(getApplicationContext()))
                    loadMenu();
                else {
                    Toast.makeText(getApplicationContext(), "Please check your connection !!", Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                    return;
                }
            }
        });
        //set Name user
        View headerView = navigationView.getHeaderView(0);
        txt_NameUser = headerView.findViewById(R.id.txt_nameuser);

        txt_NameUser.setText(Common.current_User.getName());
        txt_NameUser.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/NABILA.TTF"));

        //load Menu
        rw_menu = findViewById(R.id.rw_menu);
        GridLayoutManager layoutManager = new GridLayoutManager(this,2);
        rw_menu.setLayoutManager(layoutManager);

        LayoutAnimationController controller= AnimationUtils.loadLayoutAnimation(this,R.anim.layout_fall_down);
        rw_menu.setLayoutAnimation(controller);

        updateToken(FirebaseInstanceId.getInstance().getToken());

        //setup slider layout
        setupSlider();
    }

    private void setupSlider() {
        sliderLayout=findViewById(R.id.slider_layout);
        listImage=new HashMap<>();

        banners=db.getReference("Banner");
        banners.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot postsnapshot:dataSnapshot.getChildren()){
                    Banner banner=postsnapshot.getValue(Banner.class);
                    listImage.put(banner.getName()+"_"+banner.getId(),banner.getImage());
                }
                for (String key:listImage.keySet()){
                    String[] keySplit=key.split("_");
                    String nameofFood=keySplit[0];
                    String idofFood=keySplit[1];

                    //create slider
                    final TextSliderView textSliderView=new TextSliderView(getBaseContext());
                    textSliderView.description(nameofFood)
                            .image(listImage.get(key))
                            .setScaleType(BaseSliderView.ScaleType.Fit)
                            .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                @Override
                                public void onSliderClick(BaseSliderView slider) {
                                    Intent intent=new Intent(Home.this,FoodDetail.class);
                                    intent.putExtras(textSliderView.getBundle());
                                    startActivity(intent);

                                }
                            });
                    //add extra bundle
                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle().putString("FoodId",idofFood);
                    sliderLayout.addSlider(textSliderView);

                    //remove event after finish
                    banners.removeEventListener(this);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Background2Foreground);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        sliderLayout.setCustomAnimation(new DescriptionAnimation());
        sliderLayout.setDuration(4000);
    }

    private void updateToken(String token) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference tokens = db.getReference("Tokens");
        Token data = new Token(token, false);
        tokens.child(Common.current_User.getPhone()).setValue(data);
    }

    private void loadMenu() {

        adapter.startListening();
        rw_menu.setAdapter(adapter);
        swipeRefreshLayout.setRefreshing(false);

        //Animation
        rw_menu.getAdapter().notifyDataSetChanged();
        rw_menu.scheduleLayoutAnimation();
    }

    @Override
    protected void onStart() {
        super.onStart();
        sliderLayout.startAutoCycle();
        if (adapter != null)
            adapter.startListening();
    }

    @Override
    protected void onStop() {
        if (adapter != null)
            adapter.stopListening();
        sliderLayout.stopAutoCycle();
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        txt_NameUser.setText(Common.current_User.getName());
        fab.setCount(new Database(getApplicationContext()).getCountCart(Common.current_User.getPhone()));
        if (adapter != null)
            adapter.startListening();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_search) {
            startActivity(new Intent(Home.this,SearchActiviry.class));
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_menu) {
            // Handle the camera action
        } else if (id==R.id.nav_favorite){
            startActivity(new Intent(Home.this, FavoriteActivity.class));
        } else if(id==R.id.nav_home_address){
            showHomeAddressDialog();
        }
        else if (id == R.id.nav_cart) {
            startActivity(new Intent(Home.this, Cart.class));

        }
        else if (id == R.id.nav_order) {
            startActivity(new Intent(Home.this, OrderStatus.class));

        } else if (id==R.id.nav_setting){
            showSettingDialog();
        }
        else if (id == R.id.nav_update_name) {
            updateNameDialog();

        } else if (id == R.id.nav_logout) {
            //account kit logout
            AccountKit.logOut();
            //logout
            Intent signOut = new Intent(Home.this, MainActivity.class);
            signOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(signOut);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showSettingDialog() {
        final AlertDialog.Builder builder=new AlertDialog.Builder(Home.this);
        builder.setTitle("SETTINGS");
        builder.setMessage("Please fill all information");

        LayoutInflater inflater=LayoutInflater.from(this);
        View setting_layout=inflater.inflate(R.layout.setting_layout,null);

        final CheckBox cb_sub_news=setting_layout.findViewById(R.id.cb_sub_news);
        //remember state of checkbox
        Paper.init(Home.this);
        String is_subscribe=Paper.book().read("sub_new");
        if (is_subscribe==null|| TextUtils.isEmpty(is_subscribe) || is_subscribe.equals("false"))
            cb_sub_news.setChecked(false);
        else
            cb_sub_news.setChecked(true);



        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (cb_sub_news.isChecked()){
                    FirebaseMessaging.getInstance().subscribeToTopic(Common.topicName);
                    //write value
                    Paper.book().write("sub_new","true");
                }else {
                    FirebaseMessaging.getInstance().unsubscribeFromTopic(Common.topicName);
                    //write value
                    Paper.book().write("sub_new","false");
                }

            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setView(setting_layout);
        builder.show();
    }

    private void showHomeAddressDialog() {
        final AlertDialog.Builder builder=new AlertDialog.Builder(Home.this);
        builder.setTitle("CHANGE HOME ADDRESS");
        builder.setMessage("Please fill all information");

        LayoutInflater inflater=LayoutInflater.from(this);
        View home_address_layout=inflater.inflate(R.layout.home_address_layout,null);

        final MaterialEditText edt_home_address=home_address_layout.findViewById(R.id.edt_home_address);

        builder.setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();

                //set Home address
                Common.current_User.setHomeAddress(edt_home_address.getText().toString());
                FirebaseDatabase.getInstance().getReference("Users")
                        .child(Common.current_User.getPhone())
                        .setValue(Common.current_User)
                        .addOnCanceledListener(new OnCanceledListener() {
                            @Override
                            public void onCanceled() {
                                Toast.makeText(Home.this, "Update address successful !", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.setView(home_address_layout);
        builder.show();
    }

    private void updateNameDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("UPDATE NAME");
        alertDialog.setMessage("Please fill all information");

        LayoutInflater inflater = LayoutInflater.from(this);
        View layout_update_name = inflater.inflate(R.layout.update_name_layout, null);

        final MaterialEditText edt_update_name = layout_update_name.findViewById(R.id.edt_update_name);

        alertDialog.setView(layout_update_name);

        //event button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //change password here
                final android.app.AlertDialog waiting = new SpotsDialog.Builder().setContext(Home.this).build();
                waiting.show();

                //update name
                Map<String,Object>  name=new HashMap<>();
                name.put("name",edt_update_name.getText().toString());

                FirebaseDatabase.getInstance()
                        .getReference("Users")
                        .child(Common.current_User.getPhone())
                        .updateChildren(name)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                //dismiss dialog
                                waiting.dismiss();
                                if (task.isSuccessful())
                                    Toast.makeText(Home.this, "Name was updated", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
        alertDialog.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        alertDialog.show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
