package com.example.vanthang.foodapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanthang.foodapp.Database.Database;
import com.example.vanthang.foodapp.FoodDetail;
import com.example.vanthang.foodapp.Interface.ItemOnClickListener;
import com.example.vanthang.foodapp.Model.Favorite;
import com.example.vanthang.foodapp.Model.Order;
import com.example.vanthang.foodapp.R;
import com.example.vanthang.foodapp.Utils.Common;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewHolder> {
    Context context;
    List<Favorite> favoriteList;

    public FavoriteAdapter(Context context, List<Favorite> favoriteList) {
        this.context = context;
        this.favoriteList = favoriteList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(context).inflate(R.layout.favorite_item,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        Picasso.with(context)
                .load(favoriteList.get(i).getFoodImage())
                .into(viewHolder.img_food_favorite, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
        viewHolder.txt_name_favorite.setText(favoriteList.get(i).getFoodName());
        viewHolder.txt_price_favorite.setText(new StringBuilder("$ ").append(favoriteList.get(i).getFoodPrice()));

        viewHolder.setItemOnClickListener(new ItemOnClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                //start food detail activity
                Intent intent=new Intent(context, FoodDetail.class);
                intent.putExtra("FoodId",favoriteList.get(i).getFoodId());
                context.startActivity(intent);
            }
        });

        //add quick cart
        viewHolder.img_cart_favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isExists=new Database(context).checkFoodExists(Common.current_User.getPhone(),favoriteList.get(i).getFoodId());
                if (!isExists)
                    new Database(context).AddToCart(new Order(
                            Common.current_User.getPhone(),
                            favoriteList.get(i).getFoodId(),
                            favoriteList.get(i).getFoodName(),
                            "1",
                            favoriteList.get(i).getFoodPrice(),
                            favoriteList.get(i).getFoodDiscount(),
                            favoriteList.get(i).getFoodImage()
                    ));
                else
                    new Database(context).increaseCart(Common.current_User.getPhone(),favoriteList.get(i).getFoodId());
                Toast.makeText(context, "Added to Cart successful", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public RelativeLayout favorite_background;
        public LinearLayout  favorite_foreground;
        ImageView img_food_favorite;
        TextView  txt_name_favorite,txt_price_favorite;
        ImageView img_cart_favorite;
        ItemOnClickListener itemOnClickListener;

        public void setItemOnClickListener(ItemOnClickListener itemOnClickListener) {
            this.itemOnClickListener = itemOnClickListener;
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            favorite_foreground=itemView.findViewById(R.id.favorite_foreground);
            favorite_background=itemView.findViewById(R.id.favorite_background);
            img_food_favorite=itemView.findViewById(R.id.img_food_item);
            txt_name_favorite=itemView.findViewById(R.id.txt_food_item);
            txt_price_favorite=itemView.findViewById(R.id.txt_food_item_price);
            img_cart_favorite=itemView.findViewById(R.id.img_quick_cart);
        }

        @Override
        public void onClick(View view) {
            this.itemOnClickListener.onClick(view,getAdapterPosition(),false);
        }

    }
    public void removeItem(int position){
        favoriteList.remove(position);
        notifyItemRemoved(position);
    }
    public void restoreItem(Favorite item,int position){
        favoriteList.add(position,item);
        notifyItemInserted(position);
    }
    public Favorite getItem(int position){
        return favoriteList.get(position);
    }
}
