package com.example.vanthang.foodapp;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vanthang.foodapp.Adapter.CartAdapter;
import com.example.vanthang.foodapp.Database.Database;
import com.example.vanthang.foodapp.Helper.RecyclerItemTouchHelper;
import com.example.vanthang.foodapp.Interface.RecyclerItemTouchHelperListener;
import com.example.vanthang.foodapp.Model.DataMessage;
import com.example.vanthang.foodapp.Model.MyResponse;
import com.example.vanthang.foodapp.Model.Order;
import com.example.vanthang.foodapp.Model.Request;
import com.example.vanthang.foodapp.Model.Token;
import com.example.vanthang.foodapp.Model.User;
import com.example.vanthang.foodapp.Retrofit.APIService;
import com.example.vanthang.foodapp.Retrofit.IGoogleService;
import com.example.vanthang.foodapp.Utils.Common;
import com.example.vanthang.foodapp.Utils.Config;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Cart extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, RecyclerItemTouchHelperListener {
    private static final int PAYPAL_REQUEST_CODE = 9999;
    private static final int LOCATION_REQUEST_CODE = 8888;
    private static final int PLAY_SERVICE_REQUEST = 7777;
    FirebaseDatabase db;
    DatabaseReference requests;
    APIService mService;

    RecyclerView rw_cart;
    public TextView txt_total_price;
    Button btn_place_order;
    MaterialEditText edt_address, edt_comment;
    RelativeLayout cart_layout;
    CartAdapter cartAdapter;
    List<Order> carts = new ArrayList<>();

    //paypal
    static PayPalConfiguration configuration = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Config.PAYPAL_CLIENT_ID);
    String address, comment;

    //google place
    Place shippingAddress;

    //get location
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;

    private static final int UPDATE_INTERVAL = 1111;
    private static final int FASTEST_INTERVAL = 2222;
    private static final int DISPLACEMENT = 3333;

    //retrofit get name address
    IGoogleService mServiceGoogleMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/restaurant_font.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_cart);

        //google map service get address name
        mServiceGoogleMap = Common.getGoogleMapApi();

        //runtime permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, LOCATION_REQUEST_CODE);
        } else {
            if (checkPlayServices())//if have play service in device
            {
                buildGoogleApiClient();
                createLocationRequest();
            }
        }

        //init paypal
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, configuration);
        startService(intent);

        //init Service
        mService = Common.getFCMService();

        //init database
        db = FirebaseDatabase.getInstance();
        requests = db.getReference("Requests");


        //view
        rw_cart = findViewById(R.id.rw_cart_item);
        txt_total_price = findViewById(R.id.txt_total_price);
        btn_place_order = findViewById(R.id.btn_place_order);
        cart_layout=findViewById(R.id.cart_layout);

        rw_cart.hasFixedSize();
        rw_cart.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        //swipe delete
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback=new RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT,this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(rw_cart);

        loadItemCart();

        btn_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (carts.size() > 0)
                    showProcessOrders();
                else
                    Toast.makeText(Cart.this, "Your cart is empty !", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
                GooglePlayServicesUtil.getErrorDialog(resultCode, this, PLAY_SERVICE_REQUEST).show();
            else {
                Toast.makeText(this, "This device is not supported", Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (checkPlayServices())//if have play service in device
                    {
                        buildGoogleApiClient();
                        createLocationRequest();
                    }
                }
            }
            break;
        }
    }

    private void showProcessOrders() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Cart.this);
        builder.setTitle("One more step !");
        builder.setMessage("Enter your address: ");
        LayoutInflater inflater = this.getLayoutInflater();
        View order_comment_layout = inflater.inflate(R.layout.order_comment_layout, null);
        //edt_address = order_comment_layout.findViewById(R.id.edt_address);
        final PlaceAutocompleteFragment edt_address = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        //hide search icon before fragment
        edt_address.getView().findViewById(R.id.place_autocomplete_search_button).setVisibility(View.GONE);
        //set hint for edittext autocomplete
        ((EditText) edt_address.getView().findViewById(R.id.place_autocomplete_search_input))
                .setHint("Enter your address");
        //set text size
        ((EditText) edt_address.getView().findViewById(R.id.place_autocomplete_search_input))
                .setTextSize(14);

        //get address from place autocomplete
        edt_address.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                shippingAddress = place;
            }

            @Override
            public void onError(Status status) {
                Log.d("ANT", status.getStatusMessage());
            }
        });

        edt_comment = order_comment_layout.findViewById(R.id.edt_comment);

        //radio button
        final RadioButton rdb_shippingThisAddress = order_comment_layout.findViewById(R.id.rdb_shippingThisAddress);
        final RadioButton rdb_homeAddress = order_comment_layout.findViewById(R.id.rdb_homeAddress);
        final RadioButton rdb_cod=order_comment_layout.findViewById(R.id.rdb_cod);
        final RadioButton rdb_paypal=order_comment_layout.findViewById(R.id.rdb_payment);
        final RadioButton rdb_balance=order_comment_layout.findViewById(R.id.rdb_balance);

        //event button
        rdb_homeAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    if (!TextUtils.isEmpty(Common.current_User.getHomeAddress())| Common.current_User.getHomeAddress()!=null){
                        address=Common.current_User.getHomeAddress();
                        ((EditText) edt_address.getView().findViewById(R.id.place_autocomplete_search_input))
                                .setText(address);
                    }else
                        Toast.makeText(Cart.this, "Please update home address", Toast.LENGTH_SHORT).show();

                }
            }
        });
        rdb_shippingThisAddress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //ship to this address

                if (b) {
                    mServiceGoogleMap.getAddressName(String.format("https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&key=%s",
                            mLastLocation.getLatitude(),
                            mLastLocation.getLongitude(),
                            "AIzaSyBGytd14v4ENzQU9j0Neodh0kLxtjqFKLE"))
                            .enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {
                                    //if fetch API ok
                                    Log.d("MAP", "" + response.body());
                                    try {
                                        JSONObject jsonObject = new JSONObject(response.body());
                                        JSONArray resultArray = jsonObject.getJSONArray("results");
                                        JSONObject firstObject = resultArray.getJSONObject(0);
                                        address = firstObject.getString("formatted_address");

                                        //set this address to edt_address
                                        ((EditText) edt_address.getView().findViewById(R.id.place_autocomplete_search_input))
                                                .setText(address);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    Log.d("MAP", t.getMessage());
                                }
                            });
                }
            }
        });

        builder.setView(order_comment_layout);
        builder.setIcon(R.drawable.ic_shopping_cart_black_24dp);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //show paypal payment
                //get address and comment

                //if user select shipping this address get address from location and use
                //if user select home address get address from fragment and use
                if (!rdb_shippingThisAddress.isChecked() && !rdb_homeAddress.isChecked()) {
                    //if both select address is not checked ->
                    if (shippingAddress != null)
                        address = shippingAddress.getAddress().toString();
                    else {
                        Toast.makeText(Cart.this, "Please enter address or select option address", Toast.LENGTH_SHORT).show();
                        //fix crash fragment
                        getFragmentManager().beginTransaction()
                                .remove(getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment))
                                .commit();
                        return;
                    }
                }
                if (TextUtils.isEmpty(address)) {
                    Toast.makeText(Cart.this, "Please enter address or select option address", Toast.LENGTH_SHORT).show();
                    //fix crash fragment
                    getFragmentManager().beginTransaction()
                            .remove(getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment))
                            .commit();
                }

                comment = edt_comment.getText().toString();


                if (!rdb_cod.isChecked()&& !rdb_paypal.isChecked() && !rdb_balance.isChecked()){
                    Toast.makeText(Cart.this, "Please select payment option", Toast.LENGTH_SHORT).show();
                    //fix crash fragment
                    getFragmentManager().beginTransaction()
                            .remove(getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment))
                            .commit();
                }else if (rdb_paypal.isChecked()){
                    String toltalAmount = txt_total_price.getText().toString()
                            .replace("$", "")
                            .replace(".", "");

                    PayPalPayment payPalPayment = new PayPalPayment(new BigDecimal(toltalAmount),
                            "USD",
                            "Food App Order",
                            PayPalPayment.PAYMENT_INTENT_SALE);
                    Intent intent = new Intent(getApplicationContext(), PaymentActivity.class);
                    intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, configuration);
                    intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payPalPayment);
                    startActivityForResult(intent, PAYPAL_REQUEST_CODE);
                }else if (rdb_cod.isChecked()){
                    Request request = new Request(
                            Common.current_User.getPhone(),
                            Common.current_User.getName(),
                            address,
                            txt_total_price.getText().toString(),
                            "0",//default status
                            comment,
                            "COD",
                            "unpaid",
                            String.format("%s,%s", mLastLocation.getLatitude(),mLastLocation.getLongitude()),
                            carts
                    );

                    //submit orders to firebase
                    //using System.CurrentMilli to key
                    String order_number = String.valueOf(System.currentTimeMillis());
                    requests.child(order_number)
                            .setValue(request);

                    //Delete cart item
                    new Database(getApplicationContext()).clearCart(Common.current_User.getPhone());
                    sendNotificationOrder(order_number);
                    Toast.makeText(Cart.this, "Thank you, Order Place", Toast.LENGTH_SHORT).show();
                    finish();
                }else if (rdb_balance.isChecked()){
                    double amount =0.0;
                    //get total price from txt price
                    try {
                        amount=Common.formatCurrency(txt_total_price.getText().toString(),Locale.US).doubleValue();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    //after receive totalprice ,compare it with user balance
                    if (Double.parseDouble(Common.current_User.getBalance().toString())>= amount){
                        Request request = new Request(
                                Common.current_User.getPhone(),
                                Common.current_User.getName(),
                                address,
                                txt_total_price.getText().toString(),
                                "0",//default status
                                comment,
                                "Balance",
                                "paid",
                                String.format("%s,%s", mLastLocation.getLatitude(),mLastLocation.getLongitude()),
                                carts
                        );

                        //submit orders to firebase
                        //using System.CurrentMilli to key
                        final String order_number = String.valueOf(System.currentTimeMillis());
                        requests.child(order_number)
                                .setValue(request);
                        //Delete cart item
                        new Database(getApplicationContext()).clearCart(Common.current_User.getPhone());

                        //update balance
                        double balance=Double.parseDouble(Common.current_User.getBalance().toString())-amount;
                        Map<String,Object>update_balance=new HashMap<>();
                        update_balance.put("balance",balance);

                        FirebaseDatabase.getInstance()
                                .getReference("Users")
                                .child(Common.current_User.getPhone())
                                .updateChildren(update_balance)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()){
                                            //refresh user
                                            FirebaseDatabase.getInstance()
                                                    .getReference("Users")
                                                    .child(Common.current_User.getPhone())
                                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                            Common.current_User=dataSnapshot.getValue(User.class);
                                                            //send order to server
                                                            sendNotificationOrder(order_number);
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                        }
                                                    });
                                        }

                                    }
                                });
                    }else {
                        Toast.makeText(Cart.this, "Your balance not enough,Please choose payment method other", Toast.LENGTH_SHORT).show();
                    }

                }

                //remove fragment
                getFragmentManager().beginTransaction()
                        .remove(getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment))
                        .commit();


            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                //remove fragment
                getFragmentManager().beginTransaction()
                        .remove(getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment))
                        .commit();
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == PAYPAL_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                PaymentConfirmation confirmation = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirmation != null) try {
                    String paymentDetail = confirmation.toJSONObject().toString(4);
                    JSONObject jsonObject = new JSONObject(paymentDetail);

                    Request request = new Request(
                            Common.current_User.getPhone(),
                            Common.current_User.getName(),
                            address,
                            txt_total_price.getText().toString(),
                            "0",//default status
                            comment,
                            "Paypal",
                            jsonObject.getJSONObject("response").getString("state"),
                            String.format("%s,%s", shippingAddress.getLatLng().latitude,shippingAddress.getLatLng().longitude),
                            carts
                        );

                    //submit orders to firebase
                    //using System.CurrentMilli to key
                    String order_number = String.valueOf(System.currentTimeMillis());
                    requests.child(order_number)
                            .setValue(request);

                    //Delete cart item
                    new Database(getApplicationContext()).clearCart(Common.current_User.getPhone());
                    sendNotificationOrder(order_number);
                    Toast.makeText(this, "Thank you, Order Place", Toast.LENGTH_SHORT).show();
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Payment cancel", Toast.LENGTH_SHORT).show();
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID)
                Toast.makeText(this, "Invalid payment", Toast.LENGTH_SHORT).show();

        }
    }

    private void sendNotificationOrder(final String order_number) {
        DatabaseReference tokens = FirebaseDatabase.getInstance().getReference("Tokens");
        Query query = tokens.orderByChild("serverToken").equalTo(true);//get all node with isServerToken is true
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Token serverToken = postSnapshot.getValue(Token.class);
                    //create raw payload to send
//                    Notification notification = new Notification("Mr.Win", " You have new order " + order_number);
//                    Sender content = new Sender(serverToken.getToken(), notification);
                    Map<String,String> dataSend=new HashMap<>();
                    dataSend.put("title","Mr.Win");
                    dataSend.put("message"," You have new order " + order_number);
                    DataMessage dataMessage=new DataMessage(serverToken.getToken(),dataSend);

                    mService.sendNotification(dataMessage).enqueue(new Callback<MyResponse>() {
                        @Override
                        public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {
                            if (response.code() == 200)//just run when get result
                            {
                                if (response.body().success == 1) {
                                    Toast.makeText(Cart.this, "Order place successful", Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(Cart.this, "Failed !", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<MyResponse> call, Throwable t) {
                            Log.d("Mr.Win", t.getMessage());
                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void loadItemCart() {
        carts = new Database(this).getCarts(Common.current_User.getPhone());
        cartAdapter = new CartAdapter(this, carts);
        cartAdapter.notifyDataSetChanged();
        rw_cart.setAdapter(cartAdapter);

        int totalPrice = 0;
        for (Order order : carts)
            totalPrice += (Integer.parseInt(order.getPrice())) * (Integer.parseInt(order.getQuantity()));
        Locale locale = new Locale("en", "US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        txt_total_price.setText(fmt.format(totalPrice));

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().equals(Common.DELETE))
            deleteCart(item.getOrder());
        return true;
    }

    private void deleteCart(int position) {
        //remove item in List<Order> by position
        carts.remove(position);
        //After that ,delete all old data from SQLite
        new Database(this).clearCart(Common.current_User.getPhone());
        //and final , update new data from List<Order> to SQLite
        for (Order item : carts)
            new Database(this).AddToCart(item);

        loadItemCart();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        startLocationUpdates();
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void displayLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null)
            Log.d("LOCATION", "Your location:" + mLastLocation.getLatitude() + "," + mLastLocation.getLongitude());
        else
            Log.d("LOCATION", "Could not get your location");
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        displayLocation();
    }

    @Override
    public void onSwipe(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof CartAdapter.ViewHolder){
            String name=cartAdapter.getItem(viewHolder.getAdapterPosition()).getProductName();
            final Order order=cartAdapter.getItem(viewHolder.getAdapterPosition());
            final int itemIndex=viewHolder.getAdapterPosition();

            cartAdapter.removeItem(itemIndex);
            new Database(getApplicationContext()).deleteItemFromCart(Common.current_User.getPhone(),order.getProductId());

            //update total price
            int totalPrice = 0;
            List<Order> carts=new Database(getApplicationContext()).getCarts(Common.current_User.getPhone());
            for (Order item : carts)
                totalPrice += (Integer.parseInt(item.getPrice())) * (Integer.parseInt(item.getQuantity()));
            Locale locale = new Locale("en", "US");
            NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
            txt_total_price.setText(fmt.format(totalPrice));

            //make snackbar
            Snackbar snackbar=Snackbar.make(cart_layout,name+" removed from cart!",Snackbar.LENGTH_SHORT);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cartAdapter.restoreItem(order,itemIndex);
                    new Database(getApplicationContext()).AddToCart(order);

                    //update total price
                    int totalPrice = 0;
                    List<Order> carts=new Database(getApplicationContext()).getCarts(Common.current_User.getPhone());
                    for (Order item : carts)
                        totalPrice += (Integer.parseInt(item.getPrice())) * (Integer.parseInt(item.getQuantity()));
                    Locale locale = new Locale("en", "US");
                    NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
                    txt_total_price.setText(fmt.format(totalPrice));
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }
}
