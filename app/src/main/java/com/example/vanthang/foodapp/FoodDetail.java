package com.example.vanthang.foodapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.andremion.counterfab.CounterFab;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.vanthang.foodapp.Database.Database;
import com.example.vanthang.foodapp.Model.Food;
import com.example.vanthang.foodapp.Model.Order;
import com.example.vanthang.foodapp.Model.Rating;
import com.example.vanthang.foodapp.Utils.Common;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stepstone.apprating.AppRatingDialog;
import com.stepstone.apprating.listener.RatingDialogListener;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FoodDetail extends AppCompatActivity implements RatingDialogListener {

    ImageView img_food_detail;
    TextView txt_food_name_detail,txt_food_description,txt_food_price;
    CollapsingToolbarLayout collapsingToolbarLayout;
    CounterFab btnCart;
    ElegantNumberButton     numberButton;
    String foodId="";
    RatingBar ratingBar;
    FloatingActionButton  btn_rating;
    Button btn_show_comment;

    FirebaseDatabase    db;
    DatabaseReference   food;
    DatabaseReference   ratingTB;
    Food currentFood;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/restaurant_font.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_food_detail);

        //init database
        db=FirebaseDatabase.getInstance();
        food=db.getReference().child("Food");
        ratingTB=db.getReference("Rating");

        //init view
        img_food_detail=findViewById(R.id.img_food_detail);
        txt_food_name_detail=findViewById(R.id.txt_food_name_detail);
        txt_food_price=findViewById(R.id.txt_food_price);
        txt_food_description=findViewById(R.id.txt_food_description);
        btnCart=findViewById(R.id.btn_cart);
        numberButton=findViewById(R.id.number_food_button);
        collapsingToolbarLayout=findViewById(R.id.collapsing);
        ratingBar=findViewById(R.id.ratingBar);
        btn_rating=findViewById(R.id.btn_rating);
        btn_show_comment=findViewById(R.id.btn_showComment);

        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);

        //get food id from intent
        if (getIntent()!=null)
            foodId=getIntent().getStringExtra("FoodId");
        if (!foodId.isEmpty()) {
            if (Common.isConnectedToInternet(this)) {
                loadDetailFood(foodId);
                getRatingFood(foodId);
            }else {
                Toast.makeText(this, "Please check your connection !!", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        //setEvent
        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Database(getApplicationContext()).AddToCart(new Order(
                        Common.current_User.getPhone(),
                        foodId,
                        currentFood.getName(),
                        numberButton.getNumber(),
                        currentFood.getPrice(),
                        currentFood.getDiscount(),
                        currentFood.getImage()
                ));
                Toast.makeText(FoodDetail.this, "Added to Cart successful", Toast.LENGTH_SHORT).show();
            }
        });
        btnCart.setCount(new Database(this).getCountCart(Common.current_User.getPhone()));
        //rating
        btn_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRatingDialog();
            }
        });

        //show comment
        btn_show_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(FoodDetail.this,ShowComment.class);
                intent.putExtra(Common.FOOD_ID,foodId);
                startActivity(intent);
            }
        });
    }

    private void getRatingFood(String foodId) {
        Query foodRating=ratingTB.orderByChild("foodId").equalTo(foodId);
        foodRating.addValueEventListener(new ValueEventListener() {
            int count=0,sum=0;
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot:dataSnapshot.getChildren()){
                    Rating item=postSnapshot.getValue(Rating.class);
                    sum+=Integer.parseInt(item.getRateValues());
                    count++;
                }
                if (count!=0)
                {
                    float average=sum/count;
                    ratingBar.setRating(average);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showRatingDialog() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
                .setDefaultRating(1)
                .setTitle("Rate this food")
                .setDescription("Please select some stars and give your feedback")
                .setCommentInputEnabled(true)
                .setDefaultComment("This food is pretty cool !")
                .setStarColor(R.color.starColor)
                .setNoteDescriptionTextColor(R.color.colorPrimary)
                .setTitleTextColor(R.color.colorPrimary)
                .setDescriptionTextColor(R.color.colorPrimary)
                .setHint("Please write your comment here ...")
                .setHintTextColor(R.color.colorAccent)
                .setCommentTextColor(android.R.color.white)
                .setCommentBackgroundColor(R.color.colorPrimaryDark)
                .setWindowAnimation(R.style.MyDialogFadeAnimation)
                .setCancelable(false)
                .setCanceledOnTouchOutside(false)
                .create(FoodDetail.this)
                .show();
    }

    private void loadDetailFood(String foodId) {
        food.child(foodId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                currentFood=dataSnapshot.getValue(Food.class);

                //set view
                Picasso.with(FoodDetail.this)
                        .load(currentFood.getImage())
                        .into(img_food_detail, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });

                collapsingToolbarLayout.setTitle(currentFood.getName());
                txt_food_price.setText(currentFood.getPrice());
                txt_food_description.setText(currentFood.getDescription());
                txt_food_name_detail.setText(currentFood.getName());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("ANT",""+databaseError.getMessage());
            }
        });
    }

    @Override
    public void onNegativeButtonClicked() {

    }

    @Override
    public void onNeutralButtonClicked() {

    }

    @Override
    public void onPositiveButtonClicked(int value, @NotNull String comments) {
        //Get Rating and upload rating to firebase
        final Rating rating=new Rating(Common.current_User.getPhone(),
                foodId,
                String.valueOf(value),
                comments);
        //fix user rate multiple times
        ratingTB.push()
                .setValue(rating)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(FoodDetail.this, "Thank you for submit rating !", Toast.LENGTH_SHORT).show();
                    }
                });
        /*
        ratingTB.child(Common.current_User.getPhone()).child(foodId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(Common.current_User.getPhone()).child(foodId).exists())
                {
                    //Remove old value
                    ratingTB.child(Common.current_User.getPhone()).child(foodId).removeValue();
                    //Update new value
                    ratingTB.child(Common.current_User.getPhone()).child(foodId).setValue(rating);
                }else {
                    //Update new value
                    ratingTB.child(Common.current_User.getPhone()).child(foodId).setValue(rating);
                }
                Toast.makeText(FoodDetail.this, "Thank you for submit rating !", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        */
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
