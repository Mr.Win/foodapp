package com.example.vanthang.foodapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.vanthang.foodapp.Model.Rating;
import com.example.vanthang.foodapp.Utils.Common;
import com.example.vanthang.foodapp.ViewHolder.ShowCommentViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ShowComment extends AppCompatActivity {
    RecyclerView rw_showcomment;

    FirebaseDatabase database;
    DatabaseReference ratingTB;
    FirebaseRecyclerAdapter<Rating, ShowCommentViewHolder> adapter;

    SwipeRefreshLayout swipe_showComment;
    String Foodid = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/restaurant_font.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_show_comment);

        //database
        database = FirebaseDatabase.getInstance();
        ratingTB = database.getReference("Rating");

        //view
        rw_showcomment=findViewById(R.id.rw_showComment);
        swipe_showComment=findViewById(R.id.swipe_refresh_layout_showcomment);

        rw_showcomment.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        //swipe layout
        swipe_showComment.setColorSchemeResources(R.color.colorOrange,
                android.R.color.holo_green_dark,
                android.R.color.holo_purple,
                android.R.color.holo_red_dark,
                R.color.colorPrimary,
                android.R.color.holo_blue_dark);
        swipe_showComment.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (Common.isConnectedToInternet(getApplicationContext())) {
                    if (getIntent() != null) {
                        Foodid = getIntent().getStringExtra(Common.FOOD_ID);
                    }
                    if (!Foodid.isEmpty() && Foodid != null) {
                        loadComment(Foodid);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please check your connection !!", Toast.LENGTH_SHORT).show();
                    swipe_showComment.setRefreshing(false);
                    return;
                }
            }
        });
        //default ,load for first time
        swipe_showComment.post(new Runnable() {
            @Override
            public void run() {
                swipe_showComment.setRefreshing(true);
                if (Common.isConnectedToInternet(getApplicationContext())) {
                    if (getIntent() != null) {
                        Foodid = getIntent().getStringExtra(Common.FOOD_ID);
                    }
                    if (!Foodid.isEmpty() && Foodid != null) {
                        loadComment(Foodid);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please check your connection !!", Toast.LENGTH_SHORT).show();
                    swipe_showComment.setRefreshing(false);
                    return;
                }
            }
        });

    }

    private void loadComment(String foodid) {
        //Create request query
        Query query = ratingTB.orderByChild("foodId").equalTo(foodid);
        FirebaseRecyclerOptions<Rating> options = new FirebaseRecyclerOptions.Builder<Rating>()
                .setQuery(query, Rating.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<Rating, ShowCommentViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ShowCommentViewHolder holder, int position, @NonNull Rating model) {
                holder.ratingBar.setRating(Float.parseFloat(model.getRateValues()));
                holder.txt_comment.setText(model.getComment());
                holder.txt_userphone.setText(model.getUserPhone());
            }

            @NonNull
            @Override
            public ShowCommentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.show_comment_item_layout, viewGroup, false);
                return new ShowCommentViewHolder(view);
            }
        };
        adapter.startListening();

        rw_showcomment.setAdapter(adapter);
        swipe_showComment.setRefreshing(false);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (adapter != null)
            adapter.startListening();
    }

    @Override
    protected void onStop() {
        if (adapter != null)
            adapter.stopListening();
        super.onStop();
    }
}
