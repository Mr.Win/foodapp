package com.example.vanthang.foodapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vanthang.foodapp.Model.Order;
import com.example.vanthang.foodapp.Model.Request;
import com.example.vanthang.foodapp.Utils.Common;
import com.example.vanthang.foodapp.ViewHolder.OrderStatusViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderStatus extends AppCompatActivity {

    FirebaseDatabase db;
    DatabaseReference orderStatus;
    
    RecyclerView rw_order_status;

    FirebaseRecyclerAdapter<Request,OrderStatusViewHolder> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set font
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/restaurant_font.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_order_status);

        //init database
        db=FirebaseDatabase.getInstance();
        orderStatus=db.getReference("Requests");

        rw_order_status=findViewById(R.id.rw_order_status);
        rw_order_status.hasFixedSize();
        rw_order_status.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        loadOrderStatus(Common.current_User.getPhone());
        if (this.getIntent().getStringExtra(Common.PHONE_TEXT)== Common.current_User.getPhone())
            loadOrderStatus(Common.current_User.getPhone());
    }

    private void loadOrderStatus(String phone) {
        Query query=orderStatus.orderByChild("phone").equalTo(phone);
        FirebaseRecyclerOptions<Request> options=new FirebaseRecyclerOptions.Builder<Request>()
                .setQuery(query,Request.class)
                .build();
        adapter=new FirebaseRecyclerAdapter<Request, OrderStatusViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull OrderStatusViewHolder holder, int position, @NonNull Request model) {
                holder.txt_order_id.setText(new StringBuilder("#").append(adapter.getRef(position).getKey()));
                holder.txt_order_phone.setText(model.getPhone());
                holder.txt_order_status.setText(Common.convertCodeToStatus(model.getStatus()));
                holder.txt_order_address.setText(model.getAddress());
            }

            @NonNull
            @Override
            public OrderStatusViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View intemView=LayoutInflater.from(getApplicationContext()).inflate(R.layout.order_status_item_layout,viewGroup,false);
                return new OrderStatusViewHolder(intemView);
            }
        };
        adapter.startListening();
        rw_order_status.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (adapter!=null)
            adapter.startListening();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (adapter !=null)
            adapter.startListening();
    }

    @Override
    protected void onStop() {
        if (adapter !=null)
            adapter.stopListening();
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        if (adapter!=null)
            adapter.stopListening();
        super.onDestroy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
