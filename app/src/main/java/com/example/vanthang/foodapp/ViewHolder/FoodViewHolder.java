package com.example.vanthang.foodapp.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vanthang.foodapp.Interface.ItemOnClickListener;
import com.example.vanthang.foodapp.R;

public class FoodViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ItemOnClickListener itemOnClickListener;
    public ImageView img_food_item,img_food_favorite,img_food_share,img_quick_cart;
    public TextView txt_food_item,txt_food_item_price;

    public void setItemOnClickListener(ItemOnClickListener itemOnClickListener) {
        this.itemOnClickListener = itemOnClickListener;
    }

    public FoodViewHolder(@NonNull View itemView) {
        super(itemView);
        img_food_item=itemView.findViewById(R.id.img_food_item);
        txt_food_item=itemView.findViewById(R.id.txt_food_item);
        img_food_favorite=itemView.findViewById(R.id.img_food_favorite);
        img_food_share=itemView.findViewById(R.id.img_food_share);
        txt_food_item_price=itemView.findViewById(R.id.txt_food_item_price);
        img_quick_cart=itemView.findViewById(R.id.img_quick_cart);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        this.itemOnClickListener.onClick(view,getAdapterPosition(),false);
    }
}
