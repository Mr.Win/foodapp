package com.example.vanthang.foodapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.vanthang.foodapp.Database.Database;
import com.example.vanthang.foodapp.Interface.ItemOnClickListener;
import com.example.vanthang.foodapp.Model.Favorite;
import com.example.vanthang.foodapp.Model.Food;
import com.example.vanthang.foodapp.Model.Order;
import com.example.vanthang.foodapp.Utils.Common;
import com.example.vanthang.foodapp.ViewHolder.FoodViewHolder;
import com.facebook.CallbackManager;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mancj.materialsearchbar.MaterialSearchBar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchActiviry extends AppCompatActivity {
    RecyclerView rw_food_list;

    FirebaseDatabase db;
    DatabaseReference foods;


    FirebaseRecyclerAdapter<Food,FoodViewHolder> adapter;

    //search view
    FirebaseRecyclerAdapter<Food,FoodViewHolder> searchadapter;
    MaterialSearchBar searchBar;
    List<String> suggestList=new ArrayList<>();

    //favorites
    Database localDB;

    //Facebook share
    CallbackManager callbackManager;
    ShareDialog shareDialog;

    //Create Target from Picasso
    Target target=new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            //create photo from bitmap
            SharePhoto sharePhoto=new SharePhoto.Builder()
                    .setBitmap(bitmap)
                    .build();
            if (ShareDialog.canShow(SharePhotoContent.class)){
                SharePhotoContent content=new SharePhotoContent.Builder()
                        .addPhoto(sharePhoto)
                        .build();
                shareDialog.show(content);
            }
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }


        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/restaurant_font.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_search_activiry);

        //init Facebook share
        callbackManager=CallbackManager.Factory.create();
        shareDialog=new ShareDialog(this);

        //init database
        db=FirebaseDatabase.getInstance();
        foods=db.getReference().child("Food");

        //init localDB
        localDB=new Database(this);
        //view
        rw_food_list=findViewById(R.id.rw_seach_activity);
        rw_food_list.hasFixedSize();
        rw_food_list.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        //search bar
        searchBar=findViewById(R.id.search_food);
        loadSuggest();
        searchBar.setLastSuggestions(suggestList);
        searchBar.setCardViewElevation(8);
        searchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                List<String> suggest=new ArrayList<>();
                for (String search:suggestList)
                {
                    if (search.toLowerCase().contains(searchBar.getText().toLowerCase()))
                        suggest.add(search);
                }
                searchBar.setLastSuggestions(suggest);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        searchBar.setOnSearchActionListener(new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                //when search bar is  close
                //restore original suggest adapter
                if (!enabled)
                    rw_food_list.setAdapter(adapter);
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                //when search finish show  result search adapter
                startText(text);
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        });

        //load all foods
        loadAllFoods();
    }

    private void loadAllFoods() {
        Query query=foods;
        FirebaseRecyclerOptions<Food> options=new FirebaseRecyclerOptions.Builder<Food>()
                .setQuery(query,Food.class)
                .build();
        adapter=new FirebaseRecyclerAdapter<Food, FoodViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final FoodViewHolder holder, final int position, @NonNull final Food model) {
                Picasso.with(SearchActiviry.this)
                        .load(model.getImage())
                        .into(holder.img_food_item, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
                holder.txt_food_item.setText(model.getName());
                holder.txt_food_item_price.setText(new StringBuilder("$ ").append(model.getPrice()));

                //add favorites
                if (localDB.isFavorites(adapter.getRef(position).getKey(), Common.current_User.getPhone()))
                    holder.img_food_favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                //change status of favorite
                holder.img_food_favorite.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Favorite favorite=new Favorite();
                        favorite.setFoodId(adapter.getRef(position).getKey());
                        favorite.setFoodName(model.getName());
                        favorite.setFoodDescription(model.getDescription());
                        favorite.setFoodDiscount(model.getDiscount());
                        favorite.setFoodImage(model.getImage());
                        favorite.setFoodMenuId(model.getMenuId());
                        favorite.setFoodPrice(model.getPrice());
                        favorite.setUserPhone(Common.current_User.getPhone());
                        if (!localDB.isFavorites(adapter.getRef(position).getKey(),Common.current_User.getPhone()))
                        {
                            localDB.addToFavorites(favorite);
                            holder.img_food_favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                            Toast.makeText(SearchActiviry.this, ""+model.getName()+" was added to Favorites", Toast.LENGTH_SHORT).show();
                        }else {
                            localDB.removeFromFavorites(adapter.getRef(position).getKey(),Common.current_User.getPhone());
                            holder.img_food_favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                            Toast.makeText(SearchActiviry.this, ""+model.getName()+" was removed to Favorites", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                //add share on facebook
                holder.img_food_share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Picasso.with(getApplicationContext()).load(model.getImage())
                                .into(target);
                    }
                });

                holder.setItemOnClickListener(new ItemOnClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //start food detail activity
                        Intent intent=new Intent(SearchActiviry.this,FoodDetail.class);
                        intent.putExtra("FoodId",adapter.getRef(position).getKey());
                        startActivity(intent);
                    }
                });

                //add quick cart
                holder.img_quick_cart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        boolean isExists=new Database(getApplicationContext()).checkFoodExists(Common.current_User.getPhone(),adapter.getRef(position).getKey());
                        if (!isExists)
                            new Database(getApplicationContext()).AddToCart(new Order(
                                    Common.current_User.getPhone(),
                                    adapter.getRef(position).getKey(),
                                    model.getName(),
                                    "1",
                                    model.getPrice(),
                                    model.getDiscount(),
                                    model.getImage()
                            ));
                        else
                            new Database(getApplicationContext()).increaseCart(Common.current_User.getPhone(),adapter.getRef(position).getKey());
                        Toast.makeText(SearchActiviry.this, "Added to Cart successful", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @NonNull
            @Override
            public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView=LayoutInflater.from(getApplicationContext()).inflate(R.layout.food_item_layout,viewGroup,false);
                return new FoodViewHolder(itemView);
            }
        };
        adapter.startListening();
        rw_food_list.setAdapter(adapter);

    }

    private void startText(CharSequence text) {
        Query query=foods.orderByChild("name").equalTo(text.toString());
        FirebaseRecyclerOptions<Food> options=new FirebaseRecyclerOptions.Builder<Food>()
                .setQuery(query,Food.class)
                .build();
        searchadapter=new FirebaseRecyclerAdapter<Food, FoodViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull FoodViewHolder holder, int position, @NonNull Food model) {
                Picasso.with(SearchActiviry.this)
                        .load(model.getImage())
                        .into(holder.img_food_item, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {

                            }
                        });
                holder.txt_food_item.setText(model.getName());
                holder.setItemOnClickListener(new ItemOnClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //start food detail activity
                        Intent intent=new Intent(SearchActiviry.this,FoodDetail.class);
                        intent.putExtra("FoodId",searchadapter.getRef(position).getKey());
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View itemView= LayoutInflater.from(getApplicationContext()).inflate(R.layout.food_item_layout,viewGroup,false);
                return new FoodViewHolder(itemView);
            }
        };
        searchadapter.startListening();
        rw_food_list.setAdapter(searchadapter);
    }

    private void loadSuggest() {
        foods.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot:dataSnapshot.getChildren())
                        {
                            Food item=postSnapshot.getValue(Food.class);
                            suggestList.add(item.getName());
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStop() {
        if (adapter!=null)
            adapter.stopListening();
        if (searchadapter!=null)
            searchadapter.stopListening();
        super.onStop();
    }
}
