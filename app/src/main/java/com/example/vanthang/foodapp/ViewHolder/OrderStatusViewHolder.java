package com.example.vanthang.foodapp.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.vanthang.foodapp.Interface.ItemOnClickListener;
import com.example.vanthang.foodapp.R;

public class OrderStatusViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView txt_order_id,txt_order_status,txt_order_phone,txt_order_address;

    public OrderStatusViewHolder(@NonNull View itemView, ItemOnClickListener itemOnClickListener) {
        super(itemView);
        this.itemOnClickListener = itemOnClickListener;
    }

    ItemOnClickListener itemOnClickListener;

    public OrderStatusViewHolder(@NonNull View itemView) {
        super(itemView);
        txt_order_id=itemView.findViewById(R.id.txt_order_id);
        txt_order_status=itemView.findViewById(R.id.txt_order_status);
        txt_order_phone=itemView.findViewById(R.id.txt_order_phone);
        txt_order_address=itemView.findViewById(R.id.txt_order_address);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        this.itemOnClickListener.onClick(view,getAdapterPosition(),false);
    }
}
