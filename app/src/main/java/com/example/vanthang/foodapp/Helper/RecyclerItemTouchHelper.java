package com.example.vanthang.foodapp.Helper;

import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.example.vanthang.foodapp.Adapter.CartAdapter;
import com.example.vanthang.foodapp.Adapter.FavoriteAdapter;
import com.example.vanthang.foodapp.Interface.RecyclerItemTouchHelperListener;

public class RecyclerItemTouchHelper extends ItemTouchHelper.SimpleCallback {
    private RecyclerItemTouchHelperListener listener;

    public RecyclerItemTouchHelper(int dragDirs, int swipeDirs, RecyclerItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
        return true;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (listener!=null)
            listener.onSwipe(viewHolder,i,viewHolder.getAdapterPosition());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    @Override
    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof CartAdapter.ViewHolder){
            View view_foreground=((CartAdapter.ViewHolder)viewHolder).view_foreground;
            getDefaultUIUtil().clearView(view_foreground);
        }else if (viewHolder instanceof FavoriteAdapter.ViewHolder){
            View view_foreground=((FavoriteAdapter.ViewHolder)viewHolder).favorite_foreground;
            getDefaultUIUtil().clearView(view_foreground);
        }
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (viewHolder instanceof CartAdapter.ViewHolder){
            View view_foreground=((CartAdapter.ViewHolder)viewHolder).view_foreground;
            getDefaultUIUtil().onDraw(c,recyclerView,view_foreground,dX,dY,actionState,isCurrentlyActive);
        }else if (viewHolder instanceof FavoriteAdapter.ViewHolder){
            View view_foreground=((FavoriteAdapter.ViewHolder)viewHolder).favorite_foreground;
            getDefaultUIUtil().onDraw(c,recyclerView,view_foreground,dX,dY,actionState,isCurrentlyActive);
        }
    }

    @Override
    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder!=null){
            if (viewHolder instanceof CartAdapter.ViewHolder){
                View view_foreground=((CartAdapter.ViewHolder)viewHolder).view_foreground;
                getDefaultUIUtil().onSelected(view_foreground);
            }else if (viewHolder instanceof FavoriteAdapter.ViewHolder){
                View view_foreground=((FavoriteAdapter.ViewHolder)viewHolder).favorite_foreground;
                getDefaultUIUtil().onSelected(view_foreground);
            }
        }
    }

    @Override
    public void onChildDrawOver(@NonNull Canvas c, @NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (viewHolder instanceof CartAdapter.ViewHolder){
            View view_foreground=((CartAdapter.ViewHolder)viewHolder).view_foreground;
            getDefaultUIUtil().onDrawOver(c,recyclerView,view_foreground,dX,dY,actionState,isCurrentlyActive);
        }else if (viewHolder instanceof FavoriteAdapter.ViewHolder){
            View view_foreground=((FavoriteAdapter.ViewHolder)viewHolder).favorite_foreground;
            getDefaultUIUtil().onDrawOver(c,recyclerView,view_foreground,dX,dY,actionState,isCurrentlyActive);
        }
    }
}
