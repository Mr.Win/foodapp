package com.example.vanthang.foodapp.Database;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.vanthang.foodapp.Model.Favorite;
import com.example.vanthang.foodapp.Model.Order;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteAssetHelper {
    private static final String DB_NAME="FoodDB.db";
    private static final int DB_VER=1;

    public Database(Context context) {
        super(context, DB_NAME,null, DB_VER);
    }

    public List<Order> getCarts(String UserPhone){
        SQLiteDatabase db=getReadableDatabase();
        String[] columns={"UserPhone","ProductId","ProductName","Quantity","Price","Discount","Image"};
        Cursor cursor=db.query("OrderDetail",columns,"UserPhone=?",new String[]{UserPhone},null,null,null);

        List<Order> result=new ArrayList<>();
        if (cursor.moveToFirst())
        {
            while (!cursor.isAfterLast())
            {
                result.add(new Order(cursor.getString(cursor.getColumnIndex("UserPhone")),
                        cursor.getString(cursor.getColumnIndex("ProductId")),
                        cursor.getString(cursor.getColumnIndex("ProductName")),
                        cursor.getString(cursor.getColumnIndex("Quantity")),
                        cursor.getString(cursor.getColumnIndex("Price")),
                        cursor.getString(cursor.getColumnIndex("Discount")),
                        cursor.getString(cursor.getColumnIndex("Image"))));
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return result;
    }
    public void AddToCart(Order order)
    {
        SQLiteDatabase db=getReadableDatabase();
        String query=String.format("INSERT OR REPLACE INTO OrderDetail(UserPhone,ProductId,ProductName,Quantity,Price,Discount,Image) VALUES('%s','%s','%s','%s','%s','%s','%s');",
                order.getUserPhone(),
                order.getProductId(),
                order.getProductName(),
                order.getQuantity(),
                order.getPrice(),
                order.getDiscount(),
                order.getImage());
        db.execSQL(query);
    }
    public void clearCart(String UserPhone)
    {
        SQLiteDatabase db=getReadableDatabase();
        String query=String.format("DELETE FROM OrderDetail WHERE UserPhone='%s'",UserPhone);
        db.execSQL(query);
    }
    public int getCountCart(String UserPhone) {
        int count=0;
        SQLiteDatabase db=getReadableDatabase();
        String query=String.format("SELECT COUNT(*) FROM OrderDetail WHERE UserPhone='%s'",UserPhone);
        Cursor cursor=db.rawQuery(query,null);
        if (cursor.moveToFirst())
        {
            while (!cursor.isAfterLast())
            {
                count=cursor.getInt(0);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return count;
    }

    public void updateCart(Order order) {
        SQLiteDatabase db=getReadableDatabase();
        String query=String.format("UPDATE OrderDetail SET Quantity = '%s' WHERE UserPhone = '%s' AND ProductId='%s'",order.getQuantity(),order.getUserPhone(),order.getProductId());
        db.execSQL(query);
    }

    public boolean checkFoodExists(String UserPhone,String FoodId){
        boolean flag = false;
        SQLiteDatabase db=getReadableDatabase();
        Cursor cursor=null;
        String query=String.format("SELECT * FROM OrderDetail WHERE UserPhone = '%s' AND ProductId='%s'",UserPhone,FoodId);
        cursor=db.rawQuery(query,null);
        if (cursor.getCount()>0)
            flag= true;
        else
            flag= false;
        cursor.close();
        return flag;
    }
    public void increaseCart(String UserPhone,String FoodId){
        SQLiteDatabase db=getReadableDatabase();
        String query=String.format("UPDATE OrderDetail SET Quantity = Quantity+1 WHERE UserPhone = '%s' AND ProductId='%s'",UserPhone,FoodId);
        db.execSQL(query);
    }
    public void deleteItemFromCart(String UserPhone,String FoodId){
        SQLiteDatabase db=getReadableDatabase();
        String query=String.format("DELETE FROM OrderDetail WHERE UserPhone='%s' AND ProductId='%s'",UserPhone,FoodId);
        db.execSQL(query);
    }
    //Favorites
    public void addToFavorites(Favorite foods)
    {
        SQLiteDatabase db=getReadableDatabase();
        String query=String.format("INSERT INTO Favorites(FoodId,FoodName,FoodPrice,FoodMenuId,FoodImage,FoodDiscount,FoodDescription,UserPhone) VALUES('%s','%s','%s','%s','%s','%s','%s','%s');",
                foods.getFoodId(),
                foods.getFoodName(),
                foods.getFoodPrice(),
                foods.getFoodMenuId(),
                foods.getFoodImage(),
                foods.getFoodDiscount(),
                foods.getFoodDescription(),
                foods.getUserPhone());
        db.execSQL(query);
    }
    public List<Favorite> getAllFavorites(String UserPhone){
        SQLiteDatabase db=getReadableDatabase();
        String[] columns={"UserPhone","FoodId","FoodName","FoodPrice","FoodMenuId","FoodImage","FoodDiscount","FoodDescription"};
        Cursor cursor=db.query("Favorites",columns,"UserPhone=?",new String[]{UserPhone},null,null,null);

        List<Favorite> result=new ArrayList<>();
        if (cursor.moveToFirst())
        {
            while (!cursor.isAfterLast())
            {
                result.add(new Favorite(cursor.getString(cursor.getColumnIndex("FoodId")),
                        cursor.getString(cursor.getColumnIndex("FoodName")),
                        cursor.getString(cursor.getColumnIndex("FoodPrice")),
                        cursor.getString(cursor.getColumnIndex("FoodMenuId")),
                        cursor.getString(cursor.getColumnIndex("FoodImage")),
                        cursor.getString(cursor.getColumnIndex("FoodDiscount")),
                        cursor.getString(cursor.getColumnIndex("FoodDescription")),
                        cursor.getString(cursor.getColumnIndex("UserPhone"))));
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return result;
    }
    public void removeFromFavorites(String foodId,String userPhone)
    {
        SQLiteDatabase db=getReadableDatabase();
        String query=String.format("DELETE FROM Favorites WHERE FoodId='%s' and UserPhone='%s';",foodId,userPhone);
        db.execSQL(query);
    }
    public boolean isFavorites(String foodId,String userPhone)
    {
        SQLiteDatabase db=getReadableDatabase();
        String query=String.format("SELECT * FROM Favorites WHERE FoodId='%s' and UserPhone='%s';",foodId,userPhone);
        Cursor cursor=db.rawQuery(query,null);
        if (cursor.getCount()<=0)
        {
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }


}
